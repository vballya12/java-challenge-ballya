
package ist.challenge.ballya.model;

import javax.persistence.*;


/**
 *
 * @author Ballya Vicky Haekal
 */
import javax.persistence.Column;
import lombok.*;

@Setter
@Getter
@Entity
@Table(name = "user", schema = "public")
public class userModel {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "username", unique=true, length = 25, nullable = false)
    private String username;
    
    @Column(name = "password", length = 25, nullable = false)
    private String password;
    
}
