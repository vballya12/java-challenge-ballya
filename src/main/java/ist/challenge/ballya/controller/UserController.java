package ist.challenge.ballya.controller;

import ist.challenge.ballya.model.userModel;
import ist.challenge.ballya.service.UserService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ist.challenge.ballya.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 *
 * @author Ballya Vicky Haekal
 */
@RestController
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    UserService userService;
    @PostMapping("/register")
    @ResponseBody
    public ResponseEntity<Response> register(@RequestBody userModel request){
        
        Integer status = null;
        Response response = new Response();
        userModel result = userService.register(request);
        if(result==null){
            response.setMessage("Username Sudah Terpakai");
            status = 409;
        } else {
            response.setMessage("Sukses");
            status = 201;
        }
        response.setStatus(status);
        return  ResponseEntity
                .status(status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
    
    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<Response> login(@RequestBody userModel request){
        
        Integer status = null;
        Response response = new Response();
        if(request.getUsername()!= null 
                && request.getPassword()!= null
                && request.getUsername()!= "" 
                && request.getPassword()!= ""){
        status = userService.login(request);
        } else {
            status = 400;
        }
        if(status == 400){
            response.setMessage("Username dan / atau password kosong");
        } else if(status == 200) {
            response.setMessage("Sukses Login");
        } else if (status == 404){
            response.setMessage("Password Tidak Sesuai");
        }
        response.setStatus(status);
        return  ResponseEntity
                .status(status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
    
    @GetMapping("/list")
    public List<userModel> findAll(){
        return userService.findAll();
    }
    
    @PutMapping("/edit")
    public ResponseEntity<Response> edit(@RequestBody userModel request){
        
        Integer status = null;
        Response response = new Response();
        if(request.getUsername()!= null 
                && request.getPassword()!= null
                && request.getUsername()!= "" 
                && request.getPassword()!= ""){
        status = userService.edit(request);
        } else {
            status = 500;
        }
        if(status == 500){
            response.setMessage("Username dan / atau password kosong");
        } else if(status == 400) {
            response.setMessage("Password tidak boleh sama dengan password sebelumnya");
        } else if (status == 409){
            response.setMessage("Username sudah terpakai");
        } else if (status == 201){
            response.setMessage("Sukses");
        }
        response.setStatus(status);
        return  ResponseEntity
                .status(status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
