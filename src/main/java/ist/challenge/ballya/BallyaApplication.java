package ist.challenge.ballya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BallyaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BallyaApplication.class, args);
	}

}
