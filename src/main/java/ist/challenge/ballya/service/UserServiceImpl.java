/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ist.challenge.ballya.service;

import ist.challenge.ballya.model.userModel;
import ist.challenge.ballya.repository.UserRepository;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ballya Vicky Haekal
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    
    @Override
    public userModel create(userModel usermodel) {
        userRepository.save(usermodel);
        return usermodel;
    }

    @Override
    public userModel update(Long id, userModel usermodel) {
       userModel result = findById(id);
       if(result!=null){
           result.setUsername(usermodel.getUsername());
           result.setPassword(usermodel.getPassword());
           return userRepository.save(result);
       }
        return null;
    }

    @Override
    public List<userModel> findAll() {
        return userRepository.findAll();
    }

    @Override
    public userModel findById(Long id) {
        Optional<userModel> result = userRepository.findById(id);
        if(result.isPresent()){
            return result.get();
        }
        return null;    
    }

    @Override
    public userModel findByUsername(String username) {
        userModel result = userRepository.findByUsername(username);
        if(result!=null){
            return result;
        }
        return null; 
    }

    @Override
    public userModel register(userModel usermodel) {
        userModel result = findByUsername(usermodel.getUsername());
        if(result==null){
        userRepository.save(usermodel);
        return usermodel;
        }
        return null;
    }

    @Override
    public Integer login(userModel usermodel) {
        userModel result = findByUsername(usermodel.getUsername());
        if(result.getPassword().equals(usermodel.getPassword())){
            return 200;
        } 
        return 404;
    }

    @Override
    public Integer edit(userModel usermodel) {
        userModel result = findById(usermodel.getId());
        if(!result.getUsername().equals(usermodel.getUsername())){
        userModel cek = findByUsername(usermodel.getUsername());
        if(cek!=null){
            return 409;
            } 
        }
        if(result.getPassword().equals(usermodel.getPassword())){
            return 400;
        }
        result.setUsername(usermodel.getUsername());
        result.setPassword(usermodel.getPassword());
        return 201;
    }
}
