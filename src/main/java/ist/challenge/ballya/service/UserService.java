/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ist.challenge.ballya.service;

import ist.challenge.ballya.model.userModel;
import java.util.List;

/**
 *
 * @author Ballya Vicky Haekal
 */
public interface UserService {
    userModel create(userModel usermodel);
    userModel update(Long id, userModel usermodel);
    List<userModel> findAll();
    userModel findById(Long id);
    userModel findByUsername(String username);
    userModel register (userModel usermodel);
    Integer login (userModel usermodel);
    Integer edit (userModel usermodel);
}
