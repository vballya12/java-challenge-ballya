/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ist.challenge.ballya.response;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Ballya Vicky Haekal
 */

@Setter
@Getter
public class Response<T> {
    private Integer status;
    private String message;
}