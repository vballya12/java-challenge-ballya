package ist.challenge.ballya.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ist.challenge.ballya.model.userModel;

/**
 *
 * @author Ballya Vicky Haekal
 */
public interface UserRepository extends JpaRepository<userModel, Long> {
    
    userModel findByUsername(String username);
}
